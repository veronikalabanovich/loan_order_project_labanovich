package com.example.loanorder.constants;

public enum Code {
    ORDER_IMPOSSIBLE_TO_DELETE,
    ORDER_NOT_FOUND,
    TARIFF_NOT_FOUND,
    LOAN_CONSIDERATION,
    LOAN_ALREADY_APPROVED,
    TRY_LATER,
    INTERNAL_SERVER_ERROR
}
