package com.example.loanorder.constants;

import java.util.UUID;

public record DeleteRequest(long userId, UUID orderId) {
}
