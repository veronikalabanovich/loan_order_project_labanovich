package com.example.loanorder.controller;

import com.example.loanorder.model.Tariff;
import com.example.loanorder.response.Response;
import com.example.loanorder.response.success.SuccessResponse;
import com.example.loanorder.response.success.TariffsResponse;
import com.example.loanorder.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/loan-service")
@RequiredArgsConstructor
public class TariffController {

    private final TariffService tariffService;

    @GetMapping("/getTariffs")
    public ResponseEntity<Response> getAllTariffs() {
        List<Tariff> tariffs = tariffService.findAll();
        return new ResponseEntity<>(SuccessResponse.builder().data(TariffsResponse.builder().tariffs(tariffs).build()).build(), HttpStatus.OK);
    }
}
