package com.example.loanorder.controller;

import com.example.loanorder.constants.DeleteRequest;
import com.example.loanorder.dtoView.LoanOrderDTO;
import com.example.loanorder.dtoView.OrderDTO;
import com.example.loanorder.mapper.OrderMapper;
import com.example.loanorder.model.LoanOrder;
import com.example.loanorder.response.Response;
import com.example.loanorder.response.success.SuccessResponse;
import com.example.loanorder.service.LoanOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/loan-service")
public class LoanOrderController {

    private final LoanOrderService loanOrderService;
    private final OrderMapper mapper;

    @PostMapping("/order")
    public ResponseEntity<Response> saveLoanOrder(@RequestBody LoanOrder loanOrder) {
        LoanOrder loanOrder1 = loanOrderService.saveLoanOrder(loanOrder);
        LoanOrderDTO loanOrderDto = mapper.loanOrderToOrderDto(loanOrder1);
        return new ResponseEntity<>(SuccessResponse.builder().data(loanOrderDto).build(), HttpStatus.OK);
    }

    @GetMapping("/getStatusOrder")
    public ResponseEntity<Response> getLoanOrder(@RequestParam UUID orderId) {
        LoanOrder loanOrder = loanOrderService.getLoanOrderByIdOrder(orderId);
        OrderDTO orderDTO = mapper.orderStatusToOrderStatusDto(loanOrder);
        return new ResponseEntity<>(SuccessResponse.builder().data(orderDTO).build(), HttpStatus.OK);
    }

    @DeleteMapping("/deleteOrder")
    public void deleteLoanOrder(@RequestBody DeleteRequest deleteRequest) {
        loanOrderService.deleteLoanOrder(deleteRequest.userId(),deleteRequest.orderId());
    }
}
