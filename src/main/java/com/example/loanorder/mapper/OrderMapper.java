package com.example.loanorder.mapper;

import com.example.loanorder.dtoView.LoanOrderDTO;
import com.example.loanorder.dtoView.OrderDTO;
import com.example.loanorder.model.LoanOrder;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    LoanOrderDTO loanOrderToOrderDto(LoanOrder loanOrder);
    OrderDTO orderStatusToOrderStatusDto(LoanOrder loanOrder);
}
