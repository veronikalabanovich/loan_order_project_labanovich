package com.example.loanorder.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanOrder {

    private long id;
    private UUID orderId;
    private long userId;
    private long tariffId;
    private double creditRating;
    private String status;
    private LocalDateTime timeInsert;
    private LocalDateTime timeUpdate;
}
