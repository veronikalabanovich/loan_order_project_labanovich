package com.example.loanorder.job;

import com.example.loanorder.model.LoanOrder;
import com.example.loanorder.repository.LoanOrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@RequiredArgsConstructor
@Component
@EnableScheduling
public class Job {

    private final LoanOrderRepository loanOrderRepository;

    @Scheduled(cron = "0 */2 * * * *")
    public void jobLoanOrder() {
        List<LoanOrder> loanOrders = loanOrderRepository.getLoanOrderWithStatusInProgress();
        log.info("Start job, list orders in_progress: {}", loanOrders);
        loanOrders.forEach(S -> {
            if (ThreadLocalRandom.current().nextBoolean()) {
                S.setStatus("APPROVED");
                S.setTimeUpdate(LocalDateTime.now());
            }
            else {
                S.setStatus("REFUSED");
                S.setTimeUpdate(LocalDateTime.now());
            }
        });
        loanOrderRepository.updateStatusLoanOrder(loanOrders);
        log.info("End job, list orders approved/refused: {}", loanOrders);
    }
}
