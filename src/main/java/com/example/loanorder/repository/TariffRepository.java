package com.example.loanorder.repository;

import com.example.loanorder.model.Tariff;

import java.util.List;

public interface TariffRepository {
    List<Tariff> findAll();
    Tariff getTariffById(long id);
}
