package com.example.loanorder.repository.impl;

import com.example.loanorder.model.Tariff;
import com.example.loanorder.repository.TariffRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class TariffRepositoryImpl implements TariffRepository {

    private final NamedParameterJdbcTemplate parameterJdbcTemplate;
    private static final String GET_ALL_TARIFFS = "SELECT * FROM tariff";
    private static final String GET_TARIFF_BY_ID = "SELECT id FROM tariff" + " WHERE tariff.id = :id";

    @Override
    public List<Tariff> findAll() {
        return parameterJdbcTemplate.query(GET_ALL_TARIFFS, new BeanPropertyRowMapper<>(Tariff.class));
    }

    @Override
    public Tariff getTariffById(long id) {
        try {
            return parameterJdbcTemplate.queryForObject(GET_TARIFF_BY_ID,
                    new MapSqlParameterSource("id", id),
                    new BeanPropertyRowMapper<>(Tariff.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}

