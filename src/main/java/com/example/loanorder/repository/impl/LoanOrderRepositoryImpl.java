package com.example.loanorder.repository.impl;

import com.example.loanorder.model.LoanOrder;
import com.example.loanorder.repository.LoanOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Repository
public class LoanOrderRepositoryImpl implements LoanOrderRepository {

    private final NamedParameterJdbcTemplate parameterJdbcTemplate;
    private static final String SAVE_LOAN_ORDER =
            "INSERT INTO loan_order (order_id, user_id, tariff_id, credit_rating, status, time_insert, time_update)" +
                    " VALUES(:orderId, :userId, :tariffId, :creditRating, :status, :timeInsert, :timeUpdate)";
    private static final String GET_LOAN_ORDER_BY_ORDER_ID =
            "SELECT * FROM loan_order" +
                    " WHERE order_id = :orderId";
    private static final String DELETE_LOAN_ORDER_BY_USER_AND_ORDER_ID =
            "DELETE FROM loan_order" +
                    " WHERE order_id = :orderId AND user_id = :userId";
    private static final String GET_ALL_LOAN_ORDER_BY_USER_ID =
            "SELECT * FROM loan_order" +
                    " WHERE user_id = :userId";
    private static final String GET_LOAN_ORDER_BY_USER_AND_ORDER_ID =
            "SELECT * FROM loan_order" +
                    " WHERE order_id = :orderId AND user_id = :userId";
    private static final String GET_LOAN_ORDER_WITH_STATUS_IN_PROGRESS =
            "SELECT * FROM loan_order" +
                    " WHERE status = 'IN_PROGRESS'";
    private static final String UPDATE_STATUS_LOAN_ORDER_WITH_STATUS_IN_PROGRESS =
            "UPDATE loan_order" +
                    " SET status = :status, time_update = :timeUpdate" +
                    " WHERE status = 'IN_PROGRESS' AND user_id = :userId" +
                    " AND order_id = :orderId";


    @Override
    public LoanOrder saveLoanOrder(LoanOrder loanOrder) {
        loanOrder.setOrderId(UUID.randomUUID());
        double creditRating = (Math.random()*(0.9-0.1)) + 0.1;
        loanOrder.setCreditRating(Math.round(creditRating * 100.0)/100.0);
        loanOrder.setStatus("IN_PROGRESS");
        loanOrder.setTimeInsert(LocalDateTime.now());
        loanOrder.setTimeUpdate(LocalDateTime.now());
        parameterJdbcTemplate.update(SAVE_LOAN_ORDER, new MapSqlParameterSource()
                .addValue("orderId", loanOrder.getOrderId())
                .addValue("userId", loanOrder.getUserId())
                .addValue("tariffId", loanOrder.getTariffId())
                .addValue("creditRating", loanOrder.getCreditRating())
                .addValue("status", loanOrder.getStatus())
                .addValue("timeInsert", loanOrder.getTimeInsert())
                .addValue("timeUpdate", loanOrder.getTimeUpdate()));
        return loanOrder;
    }

    @Override
    public LoanOrder getLoanOrderByIdOrder(UUID orderId) {
        try {
        return parameterJdbcTemplate.queryForObject(GET_LOAN_ORDER_BY_ORDER_ID,
                new MapSqlParameterSource("orderId", orderId),
                new BeanPropertyRowMapper<>(LoanOrder.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void deleteLoanOrder(long userId, UUID orderId) {
        parameterJdbcTemplate.update(DELETE_LOAN_ORDER_BY_USER_AND_ORDER_ID,
                new MapSqlParameterSource()
                        .addValue("userId", userId)
                        .addValue("orderId", orderId));
    }

    @Override
    public List<LoanOrder> getLoanOrderByIdUser(long userId) {
        return parameterJdbcTemplate.query(GET_ALL_LOAN_ORDER_BY_USER_ID,
                new MapSqlParameterSource("userId", userId),
                new BeanPropertyRowMapper<>(LoanOrder.class));
    }

    @Override
    public LoanOrder getLoanOrderByOrderAndUserId(long userId, UUID orderId) {
        try {
        return parameterJdbcTemplate.queryForObject(GET_LOAN_ORDER_BY_USER_AND_ORDER_ID,
                new MapSqlParameterSource()
                        .addValue("userId", userId)
                        .addValue("orderId", orderId),
                new BeanPropertyRowMapper<>(LoanOrder.class));
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<LoanOrder> getLoanOrderWithStatusInProgress() {
        return parameterJdbcTemplate.query(GET_LOAN_ORDER_WITH_STATUS_IN_PROGRESS,
                new BeanPropertyRowMapper<>(LoanOrder.class));
    }

    @Override
    public void updateStatusLoanOrder(List<LoanOrder> updatedLoanOrder) {
        parameterJdbcTemplate.batchUpdate(UPDATE_STATUS_LOAN_ORDER_WITH_STATUS_IN_PROGRESS,
                SqlParameterSourceUtils.createBatch(updatedLoanOrder));
    }
}
