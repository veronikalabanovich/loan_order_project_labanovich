package com.example.loanorder.repository;

import com.example.loanorder.model.LoanOrder;

import java.util.List;
import java.util.UUID;

public interface LoanOrderRepository {
    LoanOrder saveLoanOrder(LoanOrder loanOrder);
    LoanOrder getLoanOrderByIdOrder(UUID orderId);
    void deleteLoanOrder(long userId, UUID orderId);
    List<LoanOrder> getLoanOrderByIdUser(long userId);
    LoanOrder getLoanOrderByOrderAndUserId(long userId, UUID orderId);
    List<LoanOrder> getLoanOrderWithStatusInProgress();
    void updateStatusLoanOrder(List<LoanOrder> updatedLoanOrder);
}
