package com.example.loanorder.service;

import com.example.loanorder.model.LoanOrder;

import java.util.UUID;

public interface LoanOrderService {
    LoanOrder saveLoanOrder(LoanOrder loanorder);
    LoanOrder getLoanOrderByIdOrder(UUID orderId);
    void deleteLoanOrder(long userId, UUID orderId);
}
