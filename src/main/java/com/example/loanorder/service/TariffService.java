package com.example.loanorder.service;

import com.example.loanorder.model.Tariff;

import java.util.List;

public interface TariffService {
    List<Tariff> findAll();
}
