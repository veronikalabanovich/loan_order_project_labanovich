package com.example.loanorder.service.impl;

import com.example.loanorder.constants.Code;
import com.example.loanorder.model.LoanOrder;
import com.example.loanorder.repository.LoanOrderRepository;
import com.example.loanorder.repository.TariffRepository;
import com.example.loanorder.response.exception.LoanOrderException;
import com.example.loanorder.service.LoanOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class LoanOrderServiceImpl implements LoanOrderService {

    private final LoanOrderRepository loanOrderRepository;
    private final TariffRepository tariffRepository;

    @Override
    public LoanOrder saveLoanOrder(LoanOrder loanorder) {
        if (tariffRepository.getTariffById(loanorder.getTariffId()) == null) {
            throw LoanOrderException.builder().code(Code.TARIFF_NOT_FOUND).message("Тариф не найден").httpStatus(HttpStatus.BAD_REQUEST).build();
        }
        List<LoanOrder> orderList = loanOrderRepository.getLoanOrderByIdUser(loanorder.getUserId());
        if (orderList.stream().anyMatch(s -> s.getStatus().equals("IN_PROGRESS") && s.getTariffId() == loanorder.getTariffId())) {
            throw LoanOrderException.builder()
                    .code(Code.LOAN_CONSIDERATION)
                    .message("Заявка на рассмотрении")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        } else if (orderList.stream().anyMatch(s -> s.getStatus().equals("APPROVED") && s.getTariffId() == loanorder.getTariffId())) {
            throw LoanOrderException.builder()
                    .code(Code.LOAN_ALREADY_APPROVED)
                    .message("Заявка на кредит одобрена")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        } else if (orderList.stream().anyMatch(s -> s.getStatus().equals("REFUSED") && s.getTariffId() == loanorder.getTariffId() &&
                ChronoUnit.MINUTES.between(s.getTimeUpdate(), LocalDateTime.now()) < 2)) {
            throw LoanOrderException.builder()
                    .code(Code.TRY_LATER)
                    .message("Попробуйте позже")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        }
        return loanOrderRepository.saveLoanOrder(loanorder);
    }

    @Override
    public LoanOrder getLoanOrderByIdOrder(UUID orderId) {
        LoanOrder loanOrder = loanOrderRepository.getLoanOrderByIdOrder(orderId);
        if (loanOrder == null) {
            throw LoanOrderException.builder()
                    .code(Code.ORDER_NOT_FOUND)
                    .message("Заявка не найдена")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        }
        return loanOrder;
    }

    @Override
    public void deleteLoanOrder(long userId, UUID orderId) {
        LoanOrder loanOrder = loanOrderRepository.getLoanOrderByOrderAndUserId(userId, orderId);
        if (loanOrder == null) {
            throw LoanOrderException.builder()
                    .code(Code.ORDER_NOT_FOUND)
                    .message("Заявка не найдена")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        }
        if(loanOrder.getStatus().equals("IN_PROGRESS")) {
            throw LoanOrderException.builder()
                    .code(Code.ORDER_IMPOSSIBLE_TO_DELETE)
                    .message("Невозможно удалить заявку")
                    .httpStatus(HttpStatus.BAD_REQUEST).build();
        }
        loanOrderRepository.deleteLoanOrder(userId, orderId);
    }
}