package com.example.loanorder.service.impl;

import com.example.loanorder.model.Tariff;
import com.example.loanorder.repository.TariffRepository;
import com.example.loanorder.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TariffServiceImpl implements TariffService {
    private final TariffRepository tariffRepository;
    @Override
    public List<Tariff> findAll() {
        return tariffRepository.findAll();
    }
}
