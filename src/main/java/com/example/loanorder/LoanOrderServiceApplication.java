package com.example.loanorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanOrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoanOrderServiceApplication.class, args);
    }

}
