package com.example.loanorder.response.exception;

import com.example.loanorder.constants.Code;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@Builder
public class LoanOrderException extends RuntimeException{
    private final Code code;
    private final String message;
    private final HttpStatus httpStatus;
}
