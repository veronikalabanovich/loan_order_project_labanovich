package com.example.loanorder.response.exception;

import com.example.loanorder.constants.Code;
import com.example.loanorder.response.error.Error;
import com.example.loanorder.response.error.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@ControllerAdvice
public class LoanOrderServiceHandler {

    @ExceptionHandler(LoanOrderException.class)
    public ResponseEntity<ErrorResponse> handleLoanOrderException(LoanOrderException ex) {
        log.error("LoanOrderException: {}", ex.toString());
        return new ResponseEntity<>(ErrorResponse.builder().error(Error.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .build()).build(), ex.getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleUnexpectedErrorException(Exception ex) {
        log.error("Exception: {}", ex.toString());
        return new ResponseEntity<>(ErrorResponse.builder().error(Error.builder().code(Code.INTERNAL_SERVER_ERROR).message("Внутренняя ошибка сервиса").build()).build(), INTERNAL_SERVER_ERROR);
    }
}

