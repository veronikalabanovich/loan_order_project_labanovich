package com.example.loanorder.tests;

import com.example.loanorder.model.LoanOrder;
import com.example.loanorder.repository.impl.LoanOrderRepositoryImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Каждый тест запускается отдельно!!!!")
@DataJdbcTest
@ActiveProfiles("test")
public class LoanOrderRepositoryTest {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Получение заявки на кредит по order_id. Ожидаемый рез-ат получен")
    public void testGetLoanOrderByIdOrder() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        LoanOrder loanOrder = loanOrderRepository.getLoanOrderByIdOrder(UUID.fromString("15d49e5f-572a-4dd6-b594-755ec23ea320"));
        assertEquals(loanOrder.getUserId(), 1);
        assertEquals(loanOrder.getStatus(), "APPROVED");
        assertEquals(loanOrder.getCreditRating(), 0.85);
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Получение заявки на кредит по order_id && user_id. Ожидаемый рез-ат получен")
    public void testGetLoanOrderByIdOrderAndIdUser() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        LoanOrder loanOrder = loanOrderRepository.getLoanOrderByOrderAndUserId(1, UUID.fromString("15d49e5f-572a-4dd6-b594-755ec23ea320"));
        assertEquals(loanOrder.getTariffId(), 2);
        assertEquals(loanOrder.getStatus(), "APPROVED");
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Получение заявки на кредит по user_id. Ожидаемый рез-ат получен")
    public void testGetLoanOrderByIdUser() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        List<LoanOrder> orderList = loanOrderRepository.getLoanOrderByIdUser(1);
        assertEquals(orderList.size(), 2);
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Получение списка заявок на кредит со статусом IN_PROGRESS. Ожидаемый рез-ат получен")
    public void testGetLoanOrderWithStatusInProgress() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        List<LoanOrder> orderList = loanOrderRepository.getLoanOrderWithStatusInProgress();
        assertEquals(orderList.size(), 1);
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Сохранение новой заявки на кредит. Ожидаемый рез-ат получен")
    public void testSaveLoanOrder() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        LoanOrder loanOrder = new LoanOrder(5, UUID.randomUUID(), 3, 3, 0.32, "IN_PROGRESS", LocalDateTime.now(), LocalDateTime.now());
        LoanOrder loanOrder1 = loanOrderRepository.saveLoanOrder(loanOrder);
        assertEquals(loanOrder1.getStatus(), loanOrder.getStatus());
        assertEquals(loanOrder1.getTariffId(), loanOrder.getTariffId());
        assertEquals(loanOrder1.getTimeInsert(), loanOrder.getTimeInsert());
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Изменение статуса заявки с IN_PROGRESS на REFUSED. Ожидаемый рез-ат получен")
    public void testUpdateLoanOrder() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        List<LoanOrder> orderList = new ArrayList<>();
        orderList.add(new LoanOrder(4,
                UUID.fromString("e35a5c44-a887-498f-b4de-1e67b2f64f5b"), 1, 3, 0.78,
                "REFUSED", LocalDateTime.now(), LocalDateTime.now()));
        loanOrderRepository.updateStatusLoanOrder(orderList);
        List<LoanOrder> orderList1 = loanOrderRepository.getLoanOrderWithStatusInProgress();
        assertEquals(orderList1.size(), 1);
    }

    @Sql("/add-loan.sql")
    @Test
    @DisplayName("Тест: Удаление заявки по order_id && user_id. Ожидаемый рез-ат получен")
    public void testDeleteLoanOrder() {
        LoanOrderRepositoryImpl loanOrderRepository = new LoanOrderRepositoryImpl(namedParameterJdbcTemplate);
        loanOrderRepository.deleteLoanOrder(1, UUID.fromString("e35a5c44-a887-498f-b4de-1e67b2f64f5b"));
        LoanOrder loanOrder = loanOrderRepository.getLoanOrderByOrderAndUserId(1, UUID.fromString("e35a5c44-a887-498f-b4de-1e67b2f64f5b"));
        assertNull(loanOrder);
    }
}
