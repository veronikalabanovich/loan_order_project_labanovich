# Сервис "Оформление кредита".

## Выпускной проект

- Исполнитель: Вероника Лабанович

## Бизнес требования

- Клиент должен иметь возможность получить тарифы и условия кредита

- Клиент должен иметь возможность подать заявку на кредит

- Клиент должен иметь возможность получить статус заявки на кредит

- Клиент должен иметь возможность удалить заявку на кредит


## Используемые технологии:
- Spring Boot 3.0.5
- Java 17
- Circuit Breaker
- PostgreSQL
- JDBCTemplate
- Lombok
- Junit5
- Maven
- Liquibase
- Docker

## API

- Метод получения тарифов
    - GET loan-service/getTariffs


- Метод подачи заявки на кредит
    - POST loan-service/order
    - Входящие данные { "userId": {userId}, "tariffId": {tariffId} }


- Метод получения статуса заявки
    - GET loan-service/getStatusOrder?orderId={orderId}


- Метод удаления заявки
    - DELETE loan-service/deleteOrder
    - Входящие данные { "userId": {userId}, "orderId": {orderId} }

## Инструкция по запуску:
1) Для запуска проекта на Вашем устройстве необходим Docker
2) Клонируете проект с gitlab в выбранную папку при помощи команды:
   - git clone https://gitlab.com/veronikalabanovich/loan_order_project_labanovich.git
3) Далее в терминале прописываете команду: chmod +x ./mvnw
4) Затем прописываете команду: docker-compose up
5) *Протестировать сервис можно в Postman